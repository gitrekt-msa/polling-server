package com.gitrekt.quora.models;

import com.arangodb.entity.DocumentField;
import com.arangodb.velocypack.annotations.Expose;
import com.arangodb.velocypack.annotations.SerializedName;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.math.BigInteger;

public class Poll {
  @DocumentField(DocumentField.Type.ID)
  String id;

  @DocumentField(DocumentField.Type.KEY)
  String key;

  String title;

  @SerializedName("user_id")
  String userId;

  Option[] options;

  public Poll() {}

  /**
   * Constructor.
   *
   * @param title The title of the poll
   * @param userId The ID of the user who created the poll
   * @param options The options of the poll
   */
  public Poll(String title, String userId, Option[] options) {
    this.title = title;
    this.userId = userId;
    this.options = options;
  }

  public String getKey() {
    return key;
  }

  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getUserId() {
    return userId;
  }

  public Option[] getOptions() {
    return options;
  }

  /**
   * Gets a JSON representation of the poll.
   *
   * @return A JSON object representing the poll
   */
  public JsonObject toJson() {
    JsonObject json = new JsonObject();
    json.addProperty("id", key);
    json.addProperty("title", title);
    JsonArray optionsJson = new JsonArray();
    for (Option option : options) {
      optionsJson.add(option.toJson());
    }
    json.add("options", optionsJson);
    return json;
  }

  public static class Option {
    String id;

    String title;

    @Expose(serialize = false, deserialize = false)
    BigInteger votes;

    public Option() {
      this.votes = BigInteger.ZERO;
    }

    /**
     * Constructor.
     *
     * @param id The id of the option
     * @param title The title of the option
     */
    public Option(String id, String title) {
      this.id = id;
      this.title = title;
      this.votes = BigInteger.ZERO;
    }

    public String getId() {
      return id;
    }

    public String getTitle() {
      return title;
    }

    public BigInteger getVotes() {
      return votes;
    }

    public void setVotes(BigInteger votes) {
      this.votes = votes;
    }

    /**
     * Gets a JSON representation of the option.
     *
     * @return A JSON object representing the option
     */
    public JsonObject toJson() {
      JsonObject json = new JsonObject();
      json.addProperty("id", id);
      json.addProperty("title", title);
      json.addProperty("votes", votes);
      return json;
    }
  }
}
