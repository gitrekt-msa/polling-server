package com.gitrekt.quora.models;

import com.arangodb.entity.DocumentField;

public class ArangoUser {
  @DocumentField(DocumentField.Type.KEY)
  String key;

  @DocumentField(DocumentField.Type.ID)
  String id;

  public ArangoUser() {}

  public ArangoUser(String key) {
    this.key = key;
  }

  public String getkey() {
    return key;
  }

  public String getId() {
    return id;
  }
}
