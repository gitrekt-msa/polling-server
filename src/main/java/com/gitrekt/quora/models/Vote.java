package com.gitrekt.quora.models;

import com.arangodb.entity.DocumentField;
import com.arangodb.velocypack.annotations.SerializedName;

public class Vote {

  @DocumentField(DocumentField.Type.FROM)
  String userId;

  @DocumentField(DocumentField.Type.TO)
  String pollId;

  @SerializedName("option_id")
  String optionId;

  public Vote() {}

  /**
   * Constructor.
   *
   * @param userId The arangoDB ID of the voting user
   * @param pollId The arangoDB ID of the poll this vote is for
   * @param optionId The ID of the option on the poll the user voted for
   */
  public Vote(String userId, String pollId, String optionId) {
    this.userId = userId;
    this.pollId = pollId;
    this.optionId = optionId;
  }

  public String getUserId() {
    return userId;
  }

  public String getPollId() {
    return pollId;
  }

  public String getOptionId() {
    return optionId;
  }
}
