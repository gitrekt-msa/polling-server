package com.gitrekt.quora.database.arango;

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.CollectionType;
import com.arangodb.model.CollectionCreateOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ArangoSeed {
  private final String seedPath;
  private final String collectionName;
  private final CollectionType collectionType;
  private final ArangoDB connection;
  private final String dbName;

  /**
   * Constructor for seed.
   *
   * @param seedPath the path of seed file.
   * @param collectionName the collection name to seed.
   * @param collectionType the type of the collection to seed.
   */
  public ArangoSeed(String seedPath, String collectionName, CollectionType collectionType) {
    connection = ArangoConnection.getInstance().getConnection();
    this.seedPath = seedPath;
    this.collectionName = collectionName;
    this.collectionType = collectionType;
    dbName = System.getenv("ARANGO_DB");

    createDatabase();
    createCollection();
  }

  /**
   * Constructor for seed for a collection of documents.
   *
   * @param seedPath the path of seed file.
   * @param collectionName the collection name to seed.
   */
  public ArangoSeed(String seedPath, String collectionName) {
    this(seedPath, collectionName, CollectionType.DOCUMENT);
  }

  /** Sample Implementation for seeding. */
  public static void main(String[] args) {
    String seedPath = "src/main/resources/database/nosql/poll/poll_seed.json";
    ArangoSeed seed = new ArangoSeed(seedPath, "polls");
    seed.seedCollection();
  }

  /** Seed the collectionName with contents of seedPath. */
  public void seedCollection() {
    JsonArray seedObjects = parseSeed();
    try {
      connection
          .db(System.getenv("ARANGO_DB"))
          .collection(collectionName)
          .importDocuments(seedObjects.toString());
    } catch (ArangoDBException exception) {
      System.out.println(exception.getMessage());
    }
  }

  /** Create Database If not exists. */
  private void createDatabase() {
    if (connection.db(dbName).exists()) {
      return;
    }

    try {
      connection.createDatabase(dbName);
    } catch (ArangoDBException exception) {
      exception.printStackTrace();
    }
  }

  /** Create the collection If not exists. */
  private void createCollection() {
    if (connection.db(dbName).collection(collectionName).exists()) {
      return;
    }

    try {
      CollectionCreateOptions options = new CollectionCreateOptions();
      options.type(collectionType);
      connection.db(dbName).createCollection(collectionName, options);
    } catch (ArangoDBException exception) {
      exception.printStackTrace();
    }
  }

  /**
   * Parse the seed file to JsonArray.
   *
   * @return JsonArray of elements;
   */
  private JsonArray parseSeed() {
    Gson gson = new Gson();
    JsonReader reader;
    try {
      reader = new JsonReader(new FileReader(seedPath));
      JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);
      return jsonObject.get(collectionName).getAsJsonArray();
    } catch (FileNotFoundException exception) {
      exception.printStackTrace();
    }
    return null;
  }
}
