package com.gitrekt.quora.database.arango;

import com.arangodb.ArangoDB;
import com.arangodb.entity.LoadBalancingStrategy;

public class ArangoConnection {
  private ArangoDB connection;

  /** Create a new Connection to ArangoDB. */
  public ArangoConnection() {
    connection =
        new ArangoDB.Builder()
            .host(System.getenv("ARANGO_HOST"), Integer.parseInt(System.getenv("ARANGO_PORT")))
            .user(System.getenv("ARANGO_USER"))
            .password(System.getenv("ARANGO_PASS"))
            .maxConnections(Integer.parseInt(System.getenv("ARANGO_POOLSIZE")))
            .loadBalancingStrategy(LoadBalancingStrategy.ROUND_ROBIN)
            .build();
  }

  public static ArangoConnection getInstance() {
    return ArangoConnectionHelper.INSTANCE;
  }

  public void closeConnection() {
    connection.shutdown();
  }

  /**
   * Changes the number of maximum connections to the arangoDB database.
   *
   * @param maxConnections the number of maximum allowed connections
   */
  public void changeMaxConnections(int maxConnections) {
    closeConnection();
    connection =
        new ArangoDB.Builder()
            .host(System.getenv("ARANGO_HOST"), Integer.parseInt(System.getenv("ARANGO_PORT")))
            .user(System.getenv("ARANGO_USER"))
            .password(System.getenv("ARANGO_PASS"))
            .maxConnections(maxConnections)
            .loadBalancingStrategy(LoadBalancingStrategy.ROUND_ROBIN)
            .build();
  }

  public ArangoDB getConnection() {
    return connection;
  }

  private static class ArangoConnectionHelper {
    private static final ArangoConnection INSTANCE = new ArangoConnection();
  }
}
