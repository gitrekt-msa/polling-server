package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoDB;
import com.gitrekt.quora.database.arango.ArangoConnection;

public abstract class ArangoHandler<T> {
  protected final ArangoDB connection;
  protected final String dbName;
  protected final String collectionName;
  protected Class<T> mapper;

  /**
   * Constructor.
   *
   * @param collectionName the collection name to handle.
   * @param mapper class to map returned objects to.
   */
  public ArangoHandler(String collectionName, Class mapper) {
    this.connection = ArangoConnection.getInstance().getConnection();
    this.dbName = System.getenv("ARANGO_DB");
    this.collectionName = collectionName;
    this.mapper = mapper;
  }
}
