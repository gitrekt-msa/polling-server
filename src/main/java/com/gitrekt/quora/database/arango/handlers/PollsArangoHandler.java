package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.DocumentCreateEntity;
import com.arangodb.model.DocumentCreateOptions;
import com.arangodb.util.MapBuilder;
import com.gitrekt.quora.models.Poll;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Map;

public class PollsArangoHandler extends ArangoHandler<Poll> {

  /** Constructor. */
  public PollsArangoHandler() {
    super("polls", Poll.class);
  }

  /**
   * Checks if a poll with the given key exists.
   *
   * @param key the key of the poll to checked for
   * @return true if the poll exists and false if it does not
   * @throws ArangoDBException An ArangoDBException
   */
  public boolean pollExists(String key) throws ArangoDBException {
    return connection.db(dbName).collection(collectionName).documentExists(key);
  }

  /**
   * Inserts a poll into the database.
   *
   * @param poll the poll to insert
   * @return a POJO representing the poll to insert
   * @throws ArangoDBException An ArangoDBException
   */
  public Poll insertPoll(Poll poll) throws ArangoDBException {
    DocumentCreateOptions options = new DocumentCreateOptions();
    options.returnNew(true);
    DocumentCreateEntity<Poll> entity =
        connection.db(dbName).collection(collectionName).insertDocument(poll, options);
    return entity.getNew();
  }

  /**
   * Gets a poll from the database.
   *
   * @param key the key of the poll
   * @return a POJO representing the poll
   * @throws ArangoDBException An ArangoDBException
   */
  public Poll getPoll(String key) throws ArangoDBException {
    return connection.db(dbName).collection(collectionName).getDocument(key, mapper);
  }

  /**
   * Gets the number of votes for the poll options which have votes.
   *
   * @param id the id of the poll to get the votes count for
   * @return a JSON object where the keys are the option ids, and the values are the number of
   *     votes, if an option has 0 votes, it is not included
   * @throws ArangoDBException An ArangoDBException
   */
  public JsonObject getPollVoteCount(String id) throws ArangoDBException {
    String query =
        "RETURN MERGE(FOR user, vote IN INBOUND @poll_id votes"
            + " COLLECT option = vote.option_id WITH COUNT INTO votes_count"
            + " RETURN {[option]: votes_count})";
    Map<String, Object> bindVars = new MapBuilder().put("poll_id", id).get();
    ArangoCursor<String> cursor = connection.db(dbName).query(query, bindVars, String.class);
    String output = cursor.first();
    Gson gson = new Gson();
    return gson.fromJson(output, JsonObject.class);
  }

  /**
   * Deletes a poll with the given key.
   *
   * @param key the key of the poll to be deleted
   * @throws ArangoDBException An ArangoDBException
   */
  public void deletePoll(String key) throws ArangoDBException {
    connection.db(dbName).collection(collectionName).deleteDocument(key);
  }
}
