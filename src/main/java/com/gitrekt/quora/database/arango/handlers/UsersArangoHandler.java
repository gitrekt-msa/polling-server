package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoDBException;
import com.arangodb.entity.DocumentCreateEntity;
import com.arangodb.model.DocumentCreateOptions;
import com.gitrekt.quora.models.ArangoUser;

public class UsersArangoHandler extends ArangoHandler<ArangoUser> {

  /** Constructor. */
  public UsersArangoHandler() {
    super("users", ArangoUser.class);
  }

  /**
   * Checks if a user exists in the ArangoDB.
   *
   * @param key The key of the user to check
   * @return true if the user exists and false if the user does not
   * @throws ArangoDBException An ArangoDBException
   */
  public boolean userExists(String key) throws ArangoDBException {
    return connection.db(dbName).collection(collectionName).documentExists(key);
  }

  /**
   * Gets a poll from the database.
   *
   * @param key the key of the user
   * @return a POJO representing the user
   * @throws ArangoDBException An ArangoDBException
   */
  public ArangoUser getUser(String key) throws ArangoDBException {
    return connection.db(dbName).collection(collectionName).getDocument(key, mapper);
  }

  /**
   * Inserts a user into the arangoDB.
   *
   * @param user The user to insert into the arangoDB
   * @throws ArangoDBException An ArangoDBException
   */
  public ArangoUser insertUser(ArangoUser user) throws ArangoDBException {
    DocumentCreateOptions options = new DocumentCreateOptions();
    options.returnNew(true);
    DocumentCreateEntity<ArangoUser> entity =
        connection.db(dbName).collection(collectionName).insertDocument(user, options);
    return entity.getNew();
  }
}
