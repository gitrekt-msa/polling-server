package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoDBException;
import com.arangodb.util.MapBuilder;
import com.gitrekt.quora.models.Vote;

import java.util.Map;

public class VotesArangoHandler extends ArangoHandler<Vote> {
  /** Constructor. */
  public VotesArangoHandler() {
    super("votes", Vote.class);
  }

  /**
   * Upsert a vote into the database. If the user hasn't voted on the poll, a new vote is created.
   * If the user has already already voted on the poll, their choice is updated
   *
   * @param vote the vote to be upserted
   * @throws ArangoDBException An ArangoDBException
   */
  public void upsertVote(Vote vote) throws ArangoDBException {

    String query =
        "UPSERT {_from: @user_id, _to: @poll_id}"
            + " INSERT {_from: @user_id, _to: @poll_id, option_id: @option_id}"
            + " UPDATE {option_id: @option_id}"
            + " IN votes";

    Map<String, Object> bindVars =
        new MapBuilder()
            .put("user_id", vote.getUserId())
            .put("poll_id", vote.getPollId())
            .put("option_id", vote.getOptionId())
            .get();
    connection.db(dbName).query(query, bindVars, mapper);
  }
}
