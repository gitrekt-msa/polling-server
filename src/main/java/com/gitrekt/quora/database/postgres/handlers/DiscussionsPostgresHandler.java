package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.Discussion;
import com.google.gson.JsonParser;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class DiscussionsPostgresHandler extends PostgresHandler<Discussion> {

  public DiscussionsPostgresHandler() {
    super("discussions", Discussion.class);
  }

  /** return a discussion with a specific ID from the database. */
  public Discussion getDiscussion(String discussionId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "SELECT * FROM discussions WHERE id = ?";
      int[] types = {Types.OTHER};
      Object[] params = {discussionId};

      ResultSet query = query(sql, types, connection, params);

      Discussion discussion = new Discussion();

      while (query.next()) {
        discussion.setId(query.getString("id"));
        discussion.setTitle(query.getString("title"));
        discussion.setBody(query.getString("body"));
        discussion.setSubscribersCount(query.getInt("subscribers_count"));
        discussion.setPublic(query.getBoolean("is_public"));
        discussion.setPollId(query.getString("poll_id"));
        discussion.setTopicId(query.getString("topic_id"));
        discussion.setUserId(query.getString("user_id"));
        discussion.setCreatedAt(query.getTimestamp("created_at"));
        discussion.setDeletedAt(query.getTimestamp("deleted_at"));
        discussion.setMedia(
            query.getString("media") == null
                ? null
                : new JsonParser().parse(query.getString("media")).getAsJsonObject());
      }
      query.close();
      connection.close();
      return discussion;
    }
  }

  /**
   * Assigns a poll to a discussion.
   *
   * @param discussionId The ID of the discussion to assign the poll to
   * @param pollId The ID of the poll to be assigned to the discussion
   * @throws SQLException An SQLException
   */
  public void assignDiscussionPoll(String discussionId, String pollId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Add_Poll_Discussion(?, ?)";
      int[] types = {Types.OTHER, Types.VARCHAR};
      Object[] params = {discussionId, pollId};

      call(sql, types, connection, params);
    }
  }

  /**
   * Clears the poll_id field for a discussion.
   *
   * @param discussionId The ID of the discussion to clear the poll_id of
   * @throws SQLException An SQLException
   */
  public void clearDiscussionPoll(String discussionId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Clear_Poll_Discussion(?)";
      int[] types = {Types.OTHER};
      Object[] params = {discussionId};

      call(sql, types, connection, params);
    }
  }
}
