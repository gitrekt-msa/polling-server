package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonParser;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class QuestionsPostgresHandler extends PostgresHandler<Question> {

  public QuestionsPostgresHandler() {
    super("questions", Question.class);
  }

  /**
   * Retrieves the question with a specified id.
   *
   * @param questionId the id of the question to be retrieved.
   * @return the resulting question.
   * @throws SQLException thrown when database error occurs.
   */
  public Question getQuestion(String questionId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "SELECT * FROM Get_Question_By_Id(?)";
      int[] types = {Types.OTHER};
      ResultSet query = query(sql, types, connection, questionId);
      Question question = new Question();
      while (query.next()) {
        question.setId(query.getString("question_id"));
        question.setUserId(query.getString("user_id"));
        question.setPollId(query.getString("poll_id"));
        question.setTitle(query.getString("title"));
        question.setBody(query.getString("body"));
        question.setMedia(new JsonParser().parse(query.getString("media")).getAsJsonObject());
        question.setCreatedAt(query.getTimestamp("created_at"));
        question.setUpdatedAt(query.getTimestamp("updated_at"));
      }
      query.close();
      connection.close();
      return question;
    }
  }

  /**
   * Assigns a poll to a question.
   *
   * @param questionId The ID of the question to assign the poll to
   * @param pollId The ID of the poll to be assigned to the question
   * @throws SQLException An SQLException
   */
  public void assignQuestionPoll(String questionId, String pollId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Add_Poll_Question(?, ?)";
      int[] types = {Types.OTHER, Types.VARCHAR};
      Object[] params = {questionId, pollId};

      call(sql, types, connection, params);
    }
  }

  /**
   * Clears the poll_id field for a question.
   *
   * @param questionId The ID of the question to clear the poll_id of
   * @throws SQLException An SQLException
   */
  public void clearQuestionPoll(String questionId) throws SQLException {
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      String sql = "CALL Clear_Poll_Question(?)";
      int[] types = {Types.OTHER};
      Object[] params = {questionId};

      call(sql, types, connection, params);
    }
  }
}
