package com.gitrekt.quora.commands;

import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Command {

  protected HashMap<String, Object> args;

  public Command(HashMap<String, Object> args) {
    this.args = args;
  }

  public abstract Object execute() throws BadRequestException, SQLException, NotFoundException;

  protected void checkArguments(String[] requiredArgs) throws BadRequestException {
    List<String> strList = new ArrayList<>();
    for (String argument : requiredArgs) {
      if (!args.containsKey(argument) || args.get(argument) == null) {
        strList.add(String.format("Argument %s is missing", argument));
      }
    }
    if (strList.size() > 0) {
      throw new BadRequestException(String.join("\n", strList));
    }
  }
}
