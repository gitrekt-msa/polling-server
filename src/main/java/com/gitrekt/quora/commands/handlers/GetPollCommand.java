package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.arango.handlers.PollsArangoHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.models.Poll;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;

public class GetPollCommand extends Command {
  private static final String[] argumentNames = new String[] {"id"};

  public GetPollCommand(HashMap<String, Object> args) {
    super(args);
  }

  /**
   * Gets a poll and the number of votes for each option.
   *
   * @return The poll with its title, ID, options, where each option contains its , title and vote
   *     count
   * @throws BadRequestException If the poll does not exist
   */
  @Override
  public Object execute() throws BadRequestException, NotFoundException {
    checkArguments(argumentNames);
    String pollId = ((JsonArray) args.get("id")).getAsString();

    PollsArangoHandler pollsArangoHandler = new PollsArangoHandler();
    Poll poll = pollsArangoHandler.getPoll(pollId);
    if (poll == null) {
      throw new NotFoundException();
    }
    JsonObject pollResults = pollsArangoHandler.getPollVoteCount(poll.getId());
    for (Poll.Option option : poll.getOptions()) {
      if (pollResults.get(option.getId()) != null) {
        option.setVotes(pollResults.get(option.getId()).getAsBigInteger());
      }
    }
    return poll.toJson();
  }
}
