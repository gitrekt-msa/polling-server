package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.arango.handlers.PollsArangoHandler;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.models.Poll;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

import java.sql.SQLException;
import java.util.HashMap;

public class CreateQuestionPollCommand extends Command {
  private static final String[] argumentNames = new String[] {"id", "userId", "title", "options"};

  public CreateQuestionPollCommand(HashMap<String, Object> args) {
    super(args);
  }

  /**
   * Create a poll for a discussion.
   *
   * @return The created poll
   * @throws BadRequestException If the user is not the creator of the question, the question
   *     already has another poll assigned to it, the provided options are less than two
   * @throws NotFoundException If the question does not exist
   * @throws SQLException If the SQL query fails
   */
  @Override
  public Object execute() throws BadRequestException, SQLException, NotFoundException {
    checkArguments(argumentNames);
    String questionId = ((JsonArray) args.get("id")).getAsString();
    String userId = (String) args.get("userId");

    QuestionsPostgresHandler questionsPostgresHandler = new QuestionsPostgresHandler();
    Question question = questionsPostgresHandler.getQuestion(questionId);
    if (question.getId() == null) {
      throw new NotFoundException();
    }
    if (!question.getUserId().equals(userId)) {
      throw new BadRequestException("You are not the creator of this question");
    }
    if (question.getPollId() != null) {
      throw new BadRequestException("A poll for this question already exists");
    }

    JsonArray optionsJson = ((JsonArray) args.get("options"));
    String[] options = new String[optionsJson.size()];
    for (int i = 0; i < optionsJson.size(); i++) {
      options[i] = optionsJson.get(i).getAsString();
    }

    if (options.length < 2) {
      throw new BadRequestException("Options count must be > 2");
    }

    Poll.Option[] pollOptions = new Poll.Option[options.length];
    for (int i = 0; i < options.length; i++) {
      pollOptions[i] = new Poll.Option(Integer.toString(i), options[i]);
    }
    String title = ((JsonPrimitive) args.get("title")).getAsString();

    Poll poll = new Poll(title, userId, pollOptions);
    PollsArangoHandler pollsArangoHandler = new PollsArangoHandler();
    Poll insertedPoll = pollsArangoHandler.insertPoll(poll);
    questionsPostgresHandler.assignQuestionPoll(questionId, poll.getKey());
    return insertedPoll.toJson();
  }
}
