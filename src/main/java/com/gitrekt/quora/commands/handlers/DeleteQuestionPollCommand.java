package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.arango.handlers.PollsArangoHandler;
import com.gitrekt.quora.database.postgres.handlers.QuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.models.Question;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;

public class DeleteQuestionPollCommand extends Command {
  private static final String[] argumentNames = new String[] {"id", "userId"};

  public DeleteQuestionPollCommand(HashMap<String, Object> args) {
    super(args);
  }

  /**
   * Deletes the poll assigned to this question.
   *
   * @return A success message if the poll was deleted
   * @throws BadRequestException If the user is not the creator of the question, or the question has
   *     no poll assigned to it
   * @throws NotFoundException If the question does not exist
   * @throws SQLException If an SQL error occurs
   */
  @Override
  public Object execute() throws BadRequestException, SQLException, NotFoundException {
    checkArguments(argumentNames);
    String questionId = ((JsonArray) args.get("id")).getAsString();
    String userId = (String) args.get("userId");

    QuestionsPostgresHandler questionsPostgresHandler = new QuestionsPostgresHandler();
    Question question = questionsPostgresHandler.getQuestion(questionId);
    if (question.getId() == null) {
      throw new NotFoundException();
    }
    if (!question.getUserId().equals(userId)) {
      throw new BadRequestException("You are not the creator of this question");
    }
    if (question.getPollId() == null) {
      throw new BadRequestException("This question has no poll");
    }

    PollsArangoHandler pollsArangoHandler = new PollsArangoHandler();
    if (pollsArangoHandler.pollExists(question.getPollId())) {
      pollsArangoHandler.deletePoll(question.getPollId());
    }
    questionsPostgresHandler.clearQuestionPoll(questionId);
    JsonObject response = new JsonObject();
    response.addProperty("message", "Poll deleted successfully");
    return response;
  }
}
