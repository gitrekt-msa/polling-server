package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.arango.handlers.PollsArangoHandler;
import com.gitrekt.quora.database.postgres.handlers.DiscussionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.models.Discussion;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;

public class DeleteDiscussionPollCommand extends Command {
  private static final String[] argumentNames = new String[] {"id", "userId"};

  public DeleteDiscussionPollCommand(HashMap<String, Object> args) {
    super(args);
  }

  /**
   * Deletes the poll assigned to this discussion.
   *
   * @return A success message if the poll was deleted
   * @throws BadRequestException If the user is not the creator of the discussion, or the discussion
   *     has no poll assigned to it @
   * @throws NotFoundException If the discussion does not exist
   * @throws SQLException If an SQL error occurs
   */
  @Override
  public Object execute() throws BadRequestException, SQLException, NotFoundException {
    checkArguments(argumentNames);
    String discussionId = ((JsonArray) args.get("id")).getAsString();
    String userId = (String) args.get("userId");

    DiscussionsPostgresHandler discussionsPostgresHandler = new DiscussionsPostgresHandler();
    Discussion discussion = discussionsPostgresHandler.getDiscussion(discussionId);
    if (discussion.getId() == null) {
      throw new NotFoundException();
    }
    if (!discussion.getUserId().equals(userId)) {
      throw new BadRequestException("You are not the creator of this discussion");
    }
    if (discussion.getPollId() == null) {
      throw new BadRequestException("This discussion has no poll");
    }

    PollsArangoHandler pollsArangoHandler = new PollsArangoHandler();
    if (pollsArangoHandler.pollExists(discussion.getPollId())) {
      pollsArangoHandler.deletePoll(discussion.getPollId());
    }
    discussionsPostgresHandler.clearDiscussionPoll(discussionId);
    JsonObject response = new JsonObject();
    response.addProperty("message", "Poll deleted successfully");
    return response;
  }
}
