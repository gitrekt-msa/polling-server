package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.arango.handlers.PollsArangoHandler;
import com.gitrekt.quora.database.arango.handlers.UsersArangoHandler;
import com.gitrekt.quora.database.arango.handlers.VotesArangoHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.ArangoUser;
import com.gitrekt.quora.models.Poll;
import com.gitrekt.quora.models.Vote;
import com.gitrekt.quora.queue.MessageQueueConnection;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

public class PutVoteCommand extends Command {

  private static final String[] argumentNames = new String[] {"userId", "id", "optionId"};

  public PutVoteCommand(HashMap<String, Object> args) {
    super(args);
  }

  /**
   * Adds a vote by a user for an option on a poll. Adds a user to the arangoDB database, then
   * upserts an edge between the user and the poll.
   *
   * @return A success message if vote is added successfully
   * @throws BadRequestException If arguments are missing, poll does not exist, or option does not
   *     exist
   */
  @Override
  public Object execute() throws BadRequestException {
    checkArguments(argumentNames);
    String pollId = ((JsonArray) args.get("id")).getAsString();
    String optionId = ((JsonPrimitive) args.get("optionId")).getAsString();

    PollsArangoHandler pollsArangoHandler = new PollsArangoHandler();
    Poll poll = pollsArangoHandler.getPoll(pollId);
    if (poll == null) {
      throw new BadRequestException("Poll does not exist");
    }
    boolean hasOption = false;
    for (Poll.Option option : poll.getOptions()) {
      if (option.getId().equals(optionId)) {
        hasOption = true;
        break;
      }
    }
    if (!hasOption) {
      throw new BadRequestException("Option does not exist for this poll");
    }
    String userId = (String) args.get("userId");
    ArangoUser user = new ArangoUser(userId);
    UsersArangoHandler usersArangoHandler = new UsersArangoHandler();
    if (!usersArangoHandler.userExists(userId)) {
      user = usersArangoHandler.insertUser(user);
    } else {
      user = usersArangoHandler.getUser(userId);
    }
    Vote vote = new Vote(user.getId(), poll.getId(), optionId);
    VotesArangoHandler votesArangoHandler = new VotesArangoHandler();
    votesArangoHandler.upsertVote(vote);
    try {
      Channel channel = MessageQueueConnection.getInstance().createChannel();
      JsonObject msg = new JsonObject();
      msg.addProperty("senderId", poll.getUserId());
      msg.addProperty("type", "poll_answered");

      channel.basicPublish("", "notification-v1-queue", null, msg.toString().getBytes());
      channel.close();

    } catch (IOException | TimeoutException exception) {
      exception.printStackTrace();
    }
    JsonObject response = new JsonObject();
    response.addProperty("message", "Vote submitted successfully");
    return response;
  }
}
