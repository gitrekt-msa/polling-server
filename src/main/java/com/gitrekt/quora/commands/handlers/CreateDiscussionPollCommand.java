package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.arango.handlers.PollsArangoHandler;
import com.gitrekt.quora.database.postgres.handlers.DiscussionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.models.Discussion;
import com.gitrekt.quora.models.Poll;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

import java.sql.SQLException;
import java.util.HashMap;

public class CreateDiscussionPollCommand extends Command {
  private static final String[] argumentNames = new String[] {"id", "userId", "title", "options"};

  public CreateDiscussionPollCommand(HashMap<String, Object> args) {
    super(args);
  }

  /**
   * Create a poll for a discussion.
   *
   * @return The created poll
   * @throws BadRequestException If the user is not the creator of the discussion, the discussion
   *     already has another poll assigned to it, the provided options are less than two
   * @throws SQLException If the SQL query fails
   * @throws NotFoundException If the discussion does not exist
   */
  @Override
  public Object execute() throws BadRequestException, SQLException, NotFoundException {
    checkArguments(argumentNames);
    String discussionId = ((JsonArray) args.get("id")).getAsString();
    String userId = (String) args.get("userId");

    DiscussionsPostgresHandler discussionsPostgresHandler = new DiscussionsPostgresHandler();
    Discussion discussion = discussionsPostgresHandler.getDiscussion(discussionId);
    if (discussion.getId() == null) {
      throw new NotFoundException();
    }
    if (!discussion.getUserId().equals(userId)) {
      throw new BadRequestException("You are not the creator of this discussion");
    }
    if (discussion.getPollId() != null) {
      throw new BadRequestException("A poll for this discussion already exists");
    }

    JsonArray optionsJson = ((JsonArray) args.get("options"));
    String[] options = new String[optionsJson.size()];
    for (int i = 0; i < optionsJson.size(); i++) {
      options[i] = optionsJson.get(i).getAsString();
    }

    if (options.length < 2) {
      throw new BadRequestException("Options count must be > 2");
    }

    Poll.Option[] pollOptions = new Poll.Option[options.length];
    for (int i = 0; i < options.length; i++) {
      pollOptions[i] = new Poll.Option(Integer.toString(i), options[i]);
    }
    String title = ((JsonPrimitive) args.get("title")).getAsString();

    Poll poll = new Poll(title, userId, pollOptions);
    PollsArangoHandler pollsArangoHandler = new PollsArangoHandler();
    Poll insertedPoll = pollsArangoHandler.insertPoll(poll);
    discussionsPostgresHandler.assignDiscussionPoll(discussionId, poll.getKey());
    return insertedPoll.toJson();
  }
}
